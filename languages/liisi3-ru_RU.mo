��            )   �      �     �     �  )   �     �     �                <     [     l     {     �     �     �     �     �     �               0  
   L     W  9   ^  ,   �  (   �     �  >        G  /  ^  N   �     �  [   �  4   E  )   z  )   �  A   �  T   	  ,   e	  %   �	  d   �	  -   
     K
  &   `
     �
  4   �
  1   �
        ,     E   I     �     �  n   �  E     O   e  '   �  �   �  )   �              	                 
                                                                                                           Annual interest rate (%) Cheatin&#8217; huh? Delay time to redirect to payment gateway Delete current picture Display Description Display Name Enable Liisi 3 part payment Enable product monthly payment Enable test mode Enable/Disable Fill Liisi credit application Image for payment logo Kuumakse al. %1$s Maximum %d seconds Order number %d Payment Settings Payment period in months Private key Private key password Product monthly payment url Public key SND ID Set interest, that number can not more than 100 or less 0 Set payment period in months, within 3 to 60 The total number of periods is %d months Upload Liisi logo picture Uploaded file is not a valid image! Only PNG files are allowed Uploaded successfully! Project-Id-Version: liisi3
PO-Revision-Date: 2022-06-10 12:24+0300
Last-Translator: Andrei Ushakov andrei@ushakov.eu
Language-Team: Русский
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.3
X-Poedit-Basepath: woocommerce-payment-gateway-liisi3
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Poedit-KeywordsList: __;_e
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.2.2; wp-5.2.2
X-Poedit-SearchPath-0: .
 Годовая процентная ставка при рассрочке (%) Нечто! Время задержки перенаправления на платежный шлюз Удалить текущее изображение Отображаемое описание Отображаемое название Использовать Liisi оплата 3-мя частями Включить предложение Liisi на странице продукта Включить тестовый режим Разрешить/Запретить Заполнить ходатайство о рассрочке платежей в 3х частях Изображение логотипа Liisi от %1$s/месяц Максимально %d секунд Заказ № %d Настройка платежной системы Период рассрочки в месяцах Приватный ключ Пароль приватного ключа Ссылка на информационную страницу (url) Открытый ключ SND ID Назначить интресс, это число должно быть в пределах от 0 до 100 Период можно выбрать от 3 до 60 месяцев. Общий период платежей составляет %d месяцев Загрузить логотип Liisi Загруженный файл не является допустимым изображением! Разрешены только файлы в формате PNG! Файл успешно загружен! 