��          �   %   �      `     a  )   z     �     �     �     �     �          (     7     U     l          �     �     �     �     �  
   �       ,     �   5  (   �     �  >        K  �  b     ?  "   X     {     �     �     �     �     �  
     %        4     D     \     p     w     �     �  0   �     �     �  0   �  �   +	     �	     �	  5   �	  "   
                                                      
                                                  	                    Annual interest rate (%) Delay time to redirect to payment gateway Delete current picture Display Description Display Name Enable Liisi 3 part payment Enable product monthly payment Enable test mode Enable/Disable Fill Liisi credit application Image for payment logo Maximum %d seconds Order number %d Payment Settings Payment period in months Private key Private key password Product monthly payment url Public key SND ID Set payment period in months, within 3 to 60 Sorry, <strong>WooCommerce %s</strong> requires WooCommerce to be installed and activated first. Please install <a href="%s">WooCommerce</a> first. The total number of periods is %d months Upload Liisi logo picture Uploaded file is not a valid image! Only PNG files are allowed Uploaded successfully! Project-Id-Version: liisi3
PO-Revision-Date: 2022-06-10 12:23+0300
Last-Translator: Andrei Ushakov andrei@ushakov.eu
Language-Team: Estonian
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.3
X-Poedit-Basepath: woocommerce-payment-gateway-liisi3
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.2.2; wp-5.2.2
X-Poedit-SearchPath-0: .
 Aasta intressimäär (%) Makseviisile suunamise viivitusaeg Kustutada praegune pilt Mooduli selgitus Maksemooduli nimi Luba Liisi maksa kolmes osas Luba Liisi kuumakse pakkumine Luba testkeskkond Luba/Keela Maksa Liisi järelmaksuga kolmes osas Liisi logo pilt Maksimaalne %d sekundit Tellimuse number %d Seaded Sisesta järelmaksu periood Privaatvõti Privaatvõtme parool Järelmaksu kampaania kirjelduse lehekülg (url) Avalik võti SND ID Järelmaksu perioodiks saad valida 3 – 60 kuud Vabandame, <strong>WooCommerce %s</strong>peab olema paigaldatud ja aktiveeritud. Palun paigaldage <a href="%s">WooCommerce</a>. järelmaksu periood %d kuud Liisi logo tõmbamine Fail on vales formaadis! Lubatud on ainult PNG-failid Fail edukalt saidile üles laetud! 